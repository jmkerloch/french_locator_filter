# {{ title }} - Documentation

> **Description:** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

----

![Demonstration of the French Locator Filter plugin for QGIS](/_static/images/french_geocoder_demo.gif "French Locator Filter - demo")

----

```{toctree}
---
caption: Manuel (Français)
maxdepth: 1
---
Installation <usage/fr_installation>
Utilisation <usage/fr_how_to_use>
```

```{toctree}
---
caption: Manual (English)
maxdepth: 1
---
Installation <usage/en_installation>
Usage <usage/en_how_to_use>
```

```{toctree}
---
caption: Project life
maxdepth: 1
---
misc/credits
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
Code documentation <_apidoc/modules>
development/contribute
development/environment
development/documentation
development/packaging
development/testing
development/history
```
