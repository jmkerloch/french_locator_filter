# Credits and sponsoring

This plugin is forked from the work of Richard Duivenvoorde, introduced on his [blog post](https://qgis.nl/2018/05/16/english-coding-a-qgslocator-plugin/?lang=en).

## Funding

```{admonition} Ready to contribute?
Plugin is free to use, not to develop. If you use it quite intensively or are interested in improving it, please consider to contribute to the code, to the documentation or fund some developments:

- [identified enhancements](https://gitlab.com/Oslandia/qgis/french_locator_filter/-/issues?scope=all&state=opened&label_name[]=enhancement)
- want to fund? Please, [send us an email](mailto:qgis@oslandia.com)
```

## Sponsors

Thanks to the sponsors:

- [Gendarmerie Nationale](https://www.gendarmerie.interieur.gouv.fr/)
- [Bordeaux Métropole](https://www.bordeaux-metropole.fr/)
