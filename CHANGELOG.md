# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.2 - 2022-04-13

- add unit test running on QGIS
- improve settings management
- update dependencies

## 1.0.1 - 2022-01-03

- minor improvments on code quality
- better documentation

## 1.0.0 - 2021-06-17

- minor fix on settings dialog UI
- stable release

## 1.0.0-beta2 - 2021-06-11

- add debug mode
- small typo fixes #5
- minor improvments

## 1.0.0-beta1 - 2021-06-02

- use QGIS 3.16 abilities (especially on networking)
- apply Oslandia code quality standards (development tooling, documentation, CI, etc.)
- add settings: display used values and allow end-user customize some preferences
- fix deprecation warning on QgsCoordinateReferenceSystem

## 0.2.0 - 2020-03-17

- adds insee code display to municipalities

## 0.1.0 - 2019-10-08

- first version
