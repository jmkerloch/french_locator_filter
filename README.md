# French Locator Plugin - QGIS Plugin

[![pipeline status](https://gitlab.com/Oslandia/qgis/french_locator_filter/badges/master/pipeline.svg)](https://gitlab.com/Oslandia/qgis/french_locator_filter/-/commits/master)
[![documentation badge](https://img.shields.io/badge/documentation-autobuilt%20with%20Sphinx-blue)](https://oslandia.gitlab.io/qgis/french_locator_filter/)

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

[:gb: Check-out the documentation - :fr: Consulter la documentation](https://oslandia.gitlab.io/qgis/french_locator_filter/)
